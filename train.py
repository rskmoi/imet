#!/usr/bin/env python
# -*- coding: utf-8 -*-
import torchvision
from builtins import range
import torch
from dataset import ImetDataset
import os
import torch.nn.functional as F
from tqdm import tqdm
import metrics
import numpy as np
import senet
from cyclic_scheduler import CyclicLRWithRestarts
from model import SEResnext50WithClassifier, SEnet154WithClassifier

def binary_focal_loss(gamma=2, **_):
    def func(input, target):
        assert target.size() == input.size()

        max_val = (-input).clamp(min=0)

        loss = input - input * target + max_val + ((-max_val).exp() + (-input - max_val).exp()).log()
        invprobs = F.logsigmoid(-input * (target * 2 - 1))
        loss = (invprobs * gamma).exp() * loss
        return loss.mean()

    return func

# Source: https://www.kaggle.com/c/human-protein-atlas-image-classification/discussion/78109
class FocalLoss(torch.nn.Module):
    def __init__(self, gamma=2):
        super().__init__()
        self.gamma = gamma

    def forward(self, input, target):
        if not (target.size() == input.size()):
            raise ValueError("Target size ({}) must be the same as input size ({})"
                             .format(target.size(), input.size()))

        max_val = (-input).clamp(min=0)
        loss = input - input * target + max_val + \
               ((-max_val).exp() + (-input - max_val).exp()).log()

        invprobs = F.logsigmoid(-input * (target * 2.0 - 1.0))
        loss = (invprobs * self.gamma).exp() * loss

        return loss.sum(dim=1).mean()

def mixup(x, y, alpha=0.2, use_cuda=True):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    lam = np.random.beta(alpha, alpha)

    batch_size = x.size()[0]
    if use_cuda:
        index = torch.randperm(batch_size).cuda()
    else:
        index = torch.randperm(batch_size)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam

def mixup_focal_loss(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)

def train_one_epoch(train_loader, model, optimizer, scheduler, criterion, epoch):
    model.train()
    train_pbar = tqdm(train_loader, total=len(train_loader))
    for sample in train_pbar:
        images, labels = sample['image'].type(torch.FloatTensor).cuda(), \
                         sample['y'].type(torch.FloatTensor).cuda()

        images, labels_a, labels_b, lam = mixup(images, labels)
        optimizer.zero_grad()
        logits = model(images)
        # loss = criterion(logits, labels)
        loss = mixup_focal_loss(criterion, logits, labels_a, labels_b, lam)
        loss.backward()
        optimizer.step()
        scheduler.batch_step()
        train_pbar.set_postfix(loss=loss.data.cpu().numpy(), epoch=epoch)

    torch.save(model.state_dict(), "result/{}epoch.pth".format(epoch))


def train():
    train_loader = ImetDataset(batch_size=8, mode="train").get_loader()
    valid_loader = ImetDataset(batch_size=128, mode="valid").get_loader()

    model = SEnet154WithClassifier()
    # model.load_state_dict(torch.load("cyclic_seresnext50/29epoch_restart_blur.pth"))
    model = model.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
    scheduler = CyclicLRWithRestarts(optimizer, 32, 98305, restart_period=2, t_mult=1.)
    criterion = FocalLoss()

    for epoch in range(100):
        scheduler.step()
        train_one_epoch(train_loader, model, optimizer, scheduler, criterion, epoch)
        with torch.no_grad():
            model.eval()
            sigmoid = torch.nn.Sigmoid()
            fbetas = []
            valid_pbar = tqdm(valid_loader, total=len(valid_loader))
            for sample in valid_pbar:
                images, labels = sample['image'].type(torch.FloatTensor).cuda(), \
                                 sample['y'].type(torch.FloatTensor).cuda()
                logits = sigmoid(model(images))
                fbeta = metrics.f2_score(labels, logits).data.cpu().numpy()
                fbetas.append(fbeta)
                valid_pbar.set_postfix(fbeta=np.mean(fbetas), epoch=epoch)

if __name__ == '__main__':
    train()
