import torch
from tqdm import tqdm
import metrics
import numpy as np
import torchvision
from dataset import ImetDataset
from sklearn.metrics import fbeta_score
import senet
from model import SEResnext50WithClassifier, SEResnext101WithClassifier
import pandas as pd

def submit(tta_zoom=1.0, img_size=256):
    model = SEResnext101WithClassifier()
    model.load_state_dict(torch.load("se_resnext101_320/23epoch.pth"))
    model = model.cuda()
    model = model.eval()
    test_loader = ImetDataset(batch_size=128, mode="test", tta_zoom=tta_zoom, img_size=img_size).get_loader()
    results = []
    inference_results = []
    with torch.no_grad():
        sigmoid = torch.nn.Sigmoid()
        valid_pbar = tqdm(test_loader, total=len(test_loader))
        for sample in valid_pbar:
            model.eval()
            images, ids = sample['image'].type(torch.FloatTensor).cuda(), sample['id']
            logits = sigmoid(model(images))
            flipped = images[:,:,:,torch.arange(img_size-1, -1, -1)]
            logits_tta = sigmoid(model(flipped))
            logits = (logits + logits_tta) / 2.
            logits_arr = logits.data.cpu().numpy()
            for i in range(len(ids)):
                results.append({"id": ids[i],
                                "attribute_ids": ' '.join(list(map(str, np.where(logits_arr[i] > 0.270)[0].tolist())))})
                inference_results.append({"id": ids[i],
                                          "logit": logits_arr[i]})


    pd.DataFrame(results)[["id", "attribute_ids"]].to_csv("./submission_0509_101_320_23ep_tta{}.csv".format(tta_zoom), index=False)
    pd.DataFrame(inference_results)[["id", "logit"]].to_pickle("./submission0509_101_320_23ep_tta_zoom_{}.pickle".format(tta_zoom))

def submit_from_pickle():
    df_10 = pd.read_pickle("./submission0504_tta_zoom_1.0.pickle")
    df_095 = pd.read_pickle("./submission0504_tta_zoom_0.95.pickle")
    df_09 = pd.read_pickle("./submission0504_tta_zoom_0.9.pickle")
    df_085 = pd.read_pickle("./submission0504_tta_zoom_0.85.pickle")
    df_08 = pd.read_pickle("./submission0504_tta_zoom_0.8.pickle")
    df_logit_mean = ((df_10["logit"] + df_095["logit"] + df_09["logit"] + df_085["logit"] + df_08["logit"]) / 5.)
    df_tta = pd.DataFrame()
    df_tta["id"] = df_10["id"]
    df_tta["logit"] = df_logit_mean

    print(df_tta.head())
    results = []
    test_pbar = tqdm(df_tta.iterrows(), total=len(df_tta))
    for _, v in test_pbar:
        pred = ' '.join(list(map(str, np.where(v["logit"] > 0.275)[0].tolist())))
        results.append({"id": v["id"],
                        "attribute_ids": pred})

    pd.DataFrame(results)[["id", "attribute_ids"]].to_csv("./submission_0504_tta_5.csv", index=False)

def ensemble():
    df_10_50 = pd.read_pickle("./submission0502_tta_zoom_1.0.pickle")
    df_095_50 = pd.read_pickle("./submission0502_tta_zoom_0.95.pickle")
    df_09_50 = pd.read_pickle("./submission0502_tta_zoom_0.9.pickle")
    df_085_50 = pd.read_pickle("./submission0502_tta_zoom_0.85.pickle")
    df_08_50 = pd.read_pickle("./submission0502_tta_zoom_0.8.pickle")
    df_10_101 = pd.read_pickle("./submission0503_tta_zoom_1.0.pickle")
    df_095_101 = pd.read_pickle("./submission0503_tta_zoom_0.95.pickle")
    df_09_101 = pd.read_pickle("./submission0503_tta_zoom_0.9.pickle")
    df_085_101 = pd.read_pickle("./submission0503_tta_zoom_0.85.pickle")
    df_08_101 = pd.read_pickle("./submission0503_tta_zoom_0.8.pickle")
    df_10_101_320 = pd.read_pickle("./submission0508_101_320_tta_zoom_1.0.pickle")
    df_095_101_320 = pd.read_pickle("./submission0508_101_320_tta_zoom_0.95.pickle")
    df_09_101_320 = pd.read_pickle("./submission0508_101_320_tta_zoom_0.9.pickle")
    df_085_101_320 = pd.read_pickle("./submission0508_101_320_tta_zoom_0.85.pickle")
    df_08_101_320 = pd.read_pickle("./submission0508_101_320_tta_zoom_0.8.pickle")
    df_10_101_320_23ep = pd.read_pickle("./submission0509_101_320_23ep_tta_zoom_1.0.pickle")
    df_095_101_320_23ep = pd.read_pickle("./submission0509_101_320_23ep_tta_zoom_0.95.pickle")
    df_09_101_320_23ep = pd.read_pickle("./submission0509_101_320_23ep_tta_zoom_0.9.pickle")
    df_085_101_320_23ep = pd.read_pickle("./submission0509_101_320_23ep_tta_zoom_0.85.pickle")
    df_08_101_320_23ep = pd.read_pickle("./submission0509_101_320_23ep_tta_zoom_0.8.pickle")
    df_10_50_320 = pd.read_pickle("./submission0507_50_320_tta_zoom_1.0.pickle")
    df_095_50_320 = pd.read_pickle("./submission0507_50_320_tta_zoom_1.0.pickle")
    df_09_50_320 = pd.read_pickle("./submission0507_50_320_tta_zoom_1.0.pickle")
    df_085_50_320 = pd.read_pickle("./submission0507_50_320_tta_zoom_1.0.pickle")
    df_08_50_320 = pd.read_pickle("./submission0507_50_320_tta_zoom_1.0.pickle")
    df_logit_mean = (df_10_50["logit"] + df_095_50["logit"] + df_09_50["logit"] + df_085_50["logit"] + df_08_50["logit"] +
                     df_10_101["logit"] + df_095_101["logit"] + df_09_101["logit"] + df_085_101["logit"] + df_08_101["logit"] +
                     df_10_101_320["logit"] + df_095_101_320["logit"] + df_09_101_320["logit"] + df_085_101_320["logit"] + df_08_101_320["logit"] +
                     df_10_101_320_23ep["logit"] + df_095_101_320_23ep["logit"] + df_09_101_320_23ep["logit"] + df_085_101_320_23ep["logit"] + df_08_101_320_23ep["logit"] +
                     df_10_50_320["logit"] + df_095_50_320["logit"] + df_09_50_320["logit"] + df_085_50_320["logit"] + df_08_50_320["logit"]) / 25.

    df_tta = pd.DataFrame()
    df_tta["id"] = df_10_50["id"]
    df_tta["logit"] = df_logit_mean

    print(df_tta.head())
    results = []
    test_pbar = tqdm(df_tta.iterrows(), total=len(df_tta))
    for _, v in test_pbar:
        pred = ' '.join(list(map(str, np.where(v["logit"] > 0.273)[0].tolist())))
        results.append({"id": v["id"],
                        "attribute_ids": pred})

    pd.DataFrame(results)[["id", "attribute_ids"]].to_csv("./submission_0509_ensemble_320_256_23ep_19epresult.csv", index=False)

if __name__ == '__main__':
    # for tta_zoom in [1.0, 0.95, 0.9, 0.85, 0.8]:
    #     submit(tta_zoom, img_size=320)
    # submit_from_pickle()
    ensemble()