import torch
from tqdm import tqdm
import metrics
import numpy as np
import torchvision
from dataset import ImetDataset
from sklearn.metrics import fbeta_score
import senet
from model import SEResnext50WithClassifier, SEResnext101WithClassifier
import pandas as pd

def valid_one_epoch(valid_loader, model, epoch, threshold=0.5, tta=None):
    model.eval()
    valid_df_base = []
    with torch.no_grad():
        sigmoid = torch.nn.Sigmoid()
        fbetas = []
        valid_pbar = tqdm(valid_loader, total=len(valid_loader))
        for sample in valid_pbar:
            images, labels = sample['image'].type(torch.FloatTensor).cuda(), \
                             sample['y'].type(torch.FloatTensor).cuda()
            # インスタ映えするTTA
            logits = sigmoid(model(images))
            flipped = images[:,:,:,torch.arange(319, -1, -1)]
            logits_tta = sigmoid(model(flipped))
            logits = (logits + logits_tta) / 2.
            fbeta = fbeta_score(y_pred=np.uint8((logits.data.cpu().numpy() > threshold)),
                                y_true=labels.data.cpu().numpy(),
                                beta=2,
                                average="samples")
            fbetas.append(fbeta)

            logits_arr = logits.data.cpu().numpy()
            labels_arr = labels.data.cpu().numpy()
            for i in range(len(labels_arr)):
                valid_df_base.append({"logit": logits_arr[i], "label": labels_arr[i]})
            valid_pbar.set_postfix(fbeta=np.mean(fbetas), epoch=epoch)

    pd.DataFrame(valid_df_base).to_pickle("./validate_result_101_320_23ep_{}.pickle".format(tta))

def validate_from_result(threshold):
    # df = pd.read_pickle("validate_result_101_320_23ep_zoom_tta.pickle")
    df = pd.read_pickle("validate_ensemble_320_256_tta.pickle")
    fbetas = []
    valid_pbar = tqdm(df.iterrows(), total=len(df))
    for _, v in valid_pbar:
        pred = np.uint8((v["logit"] > threshold))
        cur_fbeta = fbeta_score(y_true=v["label"], y_pred=pred, beta=2.)
        fbetas.append(cur_fbeta)
        valid_pbar.set_postfix(fbeta="{:4}".format(np.mean(fbetas)), threshold=threshold)

def create_tta_result():
    df_10 = pd.read_pickle("./validate_result_101_320_23ep_zoom_1.0.pickle")
    df_095 = pd.read_pickle("./validate_result_101_320_23ep_zoom_0.95.pickle")
    df_09 = pd.read_pickle("./validate_result_101_320_23ep_zoom_0.9.pickle")
    df_085 = pd.read_pickle("./validate_result_101_320_23ep_zoom_0.85.pickle")
    df_08 = pd.read_pickle("./validate_result_101_320_23ep_zoom_0.8.pickle")
    df_tta = ((df_10 + df_095 + df_09 + df_085 + df_08) / 5.)
    df_tta.to_pickle("validate_result_101_320_23ep_zoom_tta.pickle")

def ensemble():
    df_50 = pd.read_pickle("./validate_result_zoom_tta.pickle")
    df_101 = pd.read_pickle("./validate_result_101_zoom_tta.pickle")
    df_101_320 = pd.read_pickle("./validate_result_101_320_zoom_tta.pickle")
    df_101_320_23ep = pd.read_pickle("./validate_result_101_320_23ep_zoom_tta.pickle")
    df_50_320 = pd.read_pickle("./validate_result_50_320_zoom_tta.pickle")
    df_tta = (df_50 + df_101 + df_101_320 + df_101_320_23ep + df_50_320) / 5.
    # tta_320 = pd.read_pickle("validate_ensemble_50_320_101_320_tta.pickle")
    # tta_256 = pd.read_pickle("validate_ensemble_50_101_tta.pickle")
    # df_tta = ((tta_320 * 6 + tta_256 * 4) / 10.)
    df_tta.to_pickle("validate_ensemble_320_256_23ep_and_19ep_tta.pickle")

def validate():
    # model = torchvision.models.resnet18(pretrained=True)
    # model.fc = torch.nn.Linear(512, 1103)
    # model = senet.se_resnext50_32x4d(pretrained="imagenet")
    # model.avg_pool = torch.nn.AdaptiveAvgPool2d(1)
    # model.last_linear = torch.nn.Linear(512 * 4, 1103)

    # model = SEResnext101WithClassifier()
    # model = model.cuda()
    # model.load_state_dict(torch.load("se_resnext101_320/23epoch.pth"))
    # model = model.eval()
    # for tta_zoom in [1.0, 0.95, 0.9, 0.85, 0.8]:
    #    valid_loader = ImetDataset(batch_size=64, mode="valid", tta_zoom=tta_zoom, img_size=320).get_loader()
    #    valid_one_epoch(valid_loader, model, epoch=9999, threshold=0.5, tta="zoom_{}".format(tta_zoom))
    #
    # create_tta_result()
    ensemble()
    # best is 273
    for i in range(268, 276):
        validate_from_result(0.001 * i)

if __name__ == '__main__':
    validate()