import senet
import torch
class Res18():
    def __init__(self):
        pass

class SEResnext50WithClassifier(torch.nn.Module):
    def __init__(self):
        super(SEResnext50WithClassifier, self).__init__()
        self.se_resnext50 = senet.se_resnext50_32x4d(pretrained="imagenet")
        self.se_resnext50.avg_pool = torch.nn.AdaptiveAvgPool2d(1)
        self.se_resnext50.last_linear = torch.nn.Linear(512 * 4, 2048)
        self.relu = torch.nn.ReLU()
        self.dropout = torch.nn.Dropout(0.5)
        self.linear = torch.nn.Linear(2048, 1103)

    def forward(self, x):
        x = self.se_resnext50(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.linear(x)
        return x

class SEResnext101WithClassifier(torch.nn.Module):
    def __init__(self):
        super(SEResnext101WithClassifier, self).__init__()
        self.se_resnext101 = senet.se_resnext101_32x4d(pretrained="imagenet")
        self.se_resnext101.avg_pool = torch.nn.AdaptiveAvgPool2d(1)
        self.se_resnext101.last_linear = torch.nn.Linear(512 * 4, 2048)
        self.relu = torch.nn.ReLU()
        self.dropout = torch.nn.Dropout(0.5)
        self.linear = torch.nn.Linear(2048, 1103)

    def forward(self, x):
        x = self.se_resnext101(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.linear(x)
        return x

class SEnet154WithClassifier(torch.nn.Module):
    def __init__(self):
        super(SEnet154WithClassifier, self).__init__()
        self.senet154 = senet.senet154(pretrained="imagenet")
        self.senet154.avg_pool = torch.nn.AdaptiveAvgPool2d(1)
        self.senet154.last_linear = torch.nn.Linear(512 * 4, 2048)
        self.relu = torch.nn.ReLU()
        self.dropout = torch.nn.Dropout(0.5)
        self.linear = torch.nn.Linear(2048, 1103)

    def forward(self, x):
        x = self.senet154(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.linear(x)
        return x
