import pandas as pd
import hashlib
import glob
from PIL import Image

def get_hash(x):
    x_list = list(map(int, x.split(' ')))
    x_list = list(map(str, sorted(x_list)))
    x = ' '.join(x_list).encode("utf-8")
    return hashlib.sha256(x).hexdigest()

def count_corr():
    count_dict = {}

    df = pd.read_csv("../input/train.csv")
    print(df.head())

    df["hash"] = df["attribute_ids"].map(get_hash)
    print(df.head())
    print(df.nunique())

    for k, v in df.iterrows():
        if v["hash"] not in count_dict:
            count_dict[v["hash"]] = 1
        else:
            count_dict[v["hash"]] += 1

        if count_dict[v["hash"]] > 1000:
            print(v["id"])

    print(sorted(count_dict.items(), key=lambda x: x[1]))

def count_size():
    images = glob.glob("../input/train/*.png")
    for cur_image_path in images:
        cur_image = Image.open(cur_image_path)
        print(cur_image.size)

count_size()