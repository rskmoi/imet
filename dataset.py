import os
import glob
import pickle
import torch
import pandas as pd
import numpy as np
from PIL import Image
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from tqdm import tqdm
import torchvision
from collections import OrderedDict
from scipy.signal import decimate
import scipy

train_csv = "./train_split_90pc.csv"
valid_csv = "./valid_split_10pc.csv"
test_csv = "../input/test.csv"

class _Dataset(Dataset):
    def __init__(self, mode='train', tta_zoom=1.0, img_size=256):
        if mode == "train":
            self.df = pd.read_csv(train_csv)
        elif mode == "valid":
            self.df = pd.read_csv(valid_csv)
        elif mode == "test":
            self.df = self._get_test_df()
        else:
            pass

        print(len(self.df))

        self.mode = mode
        self.tta_zoom = tta_zoom
        self.img_size = img_size
        self.transform = self.make_transform(mode, tta_zoom)


    def _get_test_df(self, path="../input/test"):
        path_tests = sorted(glob.glob(os.path.join(path, "*.png")))
        df_base = []
        for cur_path in path_tests:
            df_base.append({"id": os.path.basename(cur_path).replace(".png", "")})

        return pd.DataFrame(df_base)

    def make_transform(self, mode, tta_zoom):
        if mode == "train":
            return transforms.Compose([SquarePad(),
                                       transforms.Resize((self.img_size, self.img_size)),
                                       transforms.RandomResizedCrop((self.img_size, self.img_size), scale=(0.66, 1.0), ratio=(1.0, 1.0)),
                                       transforms.RandomHorizontalFlip(),
                                       torchvision.transforms.ColorJitter(brightness=0.3, contrast=0.8, saturation=0.5, hue=0.01),
                                       # transforms.RandomAffine(0, shear=0.8),
                                       transforms.ToTensor(),
                                       transforms.Normalize(
                                           [0.485, 0.456, 0.406],
                                           [0.229, 0.224, 0.225])
                                       ])
        else:
            if tta_zoom == 1.0:
                return transforms.Compose([SquarePad(),
                                           transforms.Resize((self.img_size, self.img_size)),
                                           # transforms.CenterCrop((224, 224)),
                                           transforms.ToTensor(),
                                           transforms.Normalize(
                                               [0.485, 0.456, 0.406],
                                               [0.229, 0.224, 0.225])
                                           ])
            else:
                return transforms.Compose([SquarePad(),
                                           transforms.Resize((self.img_size, self.img_size)),
                                           transforms.CenterCrop((self.img_size * tta_zoom, self.img_size * tta_zoom)),
                                           transforms.Resize((self.img_size, self.img_size)),
                                           transforms.ToTensor(),
                                           transforms.Normalize(
                                               [0.485, 0.456, 0.406],
                                               [0.229, 0.224, 0.225])
                                           ])

    def __len__(self):
        return len(self.df)

    def make_label(self, dense_label):
        label = np.zeros((1103,), dtype=np.uint8)
        dense_label_split = dense_label.split(" ")
        for cur_label in dense_label_split:
            label[int(cur_label)] = 1.

        assert label.shape == (1103,)
        return label

    def __getitem__(self, idx):
        """
        train modeでは画像とカテゴリID、test modeでは画像とkey_idを返す
        :param idx:
        :return:
        """
        data = self.df.iloc[idx]
        sample = {}

        dir_image = "train" if self.mode in ["train", "valid"] else "test"
        img_path = "../input/{}/{}.png".format(dir_image, data["id"])
        image = Image.open(img_path)

        if self.mode == 'train':
            sample['y'] = self.make_label(data['attribute_ids'])
        elif self.mode == "valid":
            sample['y'] = self.make_label(data['attribute_ids'])
        elif self.mode == 'test':
            sample['id'] = data["id"]

        sample['image'] = self.transform(image)
        return sample

class SquarePad(object):
    def __call__(self, img):
        """
        Args:
            img (PIL Image): Image to be scaled.

        Returns:
            PIL Image: Rescaled image.
        """
        h, w = img.size[0], img.size[1]
        p = int(abs(h - w) / 2)
        if h < w:
            padding = (p, 0, p, 0)
        else:
            padding = (0, p, 0, p)
        m = torchvision.transforms.Pad(padding, padding_mode='edge')
        return m(img)

class ImetDataset():
    def __init__(self, batch_size, mode, epoch=None, tta_zoom=1.0, img_size=256):
        self.batch_size = batch_size
        self.num_workers = 4
        self.mode = mode
        self.dataset = _Dataset(mode=mode, tta_zoom=tta_zoom, img_size=img_size)
        self.batch_loader = self.init_batch_loader(epoch)

    def get_loader(self):
        loader = DataLoader(self.dataset, batch_size=self.batch_size,
                            shuffle=(self.mode == "train"), num_workers=self.num_workers)
        return loader

    def init_batch_loader(self, epoch=None):
        _epoch = 0
        while True:
            _epoch += 1
            loader = DataLoader(self.dataset, batch_size=self.batch_size,
                                shuffle=(self.mode=="train"), num_workers=self.num_workers)
            for sample in loader:
                yield sample

            if epoch is not None and _epoch == epoch:
                raise StopIteration

    def get_batch(self):
        return self.batch_loader.__next__()

if __name__ == '__main__':
    dataset = ImetDataset(batch_size=128, mode="train")
    for i in range(100):
        tensor = dataset.get_batch()
        images_arr = tensor["image"].data.numpy()
        labels_arr = tensor["y"].data.numpy()
        print(images_arr.shape)
        print(labels_arr.shape)
        for i in range(128):
            cur_images_arr = np.uint8(images_arr[i] * 255).transpose((1, 2, 0))
            cur_img = Image.fromarray(cur_images_arr)
            cur_img.save("check/{}.png".format(i))

        break

